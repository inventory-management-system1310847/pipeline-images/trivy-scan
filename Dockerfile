FROM registry.gitlab.com/iron_giant1/container-images/ubi8:20241122

# RUN trivy --version

# Install jq for JSON parsing
RUN yum install -y jq \
    && yum clean all

# Install Trivy
RUN TRIVY_VERSION=$(curl -s https://api.github.com/repos/aquasecurity/trivy/releases/latest | jq -r .tag_name) \
    && curl -L -o /tmp/trivy.tar.gz "https://github.com/aquasecurity/trivy/releases/download/${TRIVY_VERSION}/trivy_${TRIVY_VERSION:1}_Linux-64bit.tar.gz" \
    && tar -xvf /tmp/trivy.tar.gz -C /usr/local/bin/ \
    && chmod +x /usr/local/bin/trivy \
    && rm -f /tmp/trivy.tar.gz

# Set Trivy cache directory
ENV TRIVY_CACHE_DIR=/root/.cache/trivy

# Default command to display Trivy version
CMD ["trivy", "--version"]
